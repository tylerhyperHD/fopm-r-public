package net.camtech.fopmremastered.listeners;

import java.util.Iterator;
import java.util.Map;
import me.StevenLawson.BukkitTelnet.api.TelnetCommandEvent;
import me.StevenLawson.BukkitTelnet.api.TelnetPreLoginEvent;
import me.StevenLawson.BukkitTelnet.api.TelnetRequestDataTagsEvent;
import net.camtech.fopmremastered.FOPMR_Configs;
import net.camtech.fopmremastered.FOPMR_Rank;
import net.camtech.fopmremastered.FreedomOpModRemastered;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class FOPMR_TelnetListener implements Listener
{
    public FOPMR_TelnetListener()
    {
        if (!Bukkit.getPluginManager().isPluginEnabled("BukkitTelnet"))
        {
            Bukkit.broadcastMessage(ChatColor.RED + "BukkitTelnet cannot be found, disabling integration.");
            return;
        }
        Bukkit.getPluginManager().registerEvents(this, FreedomOpModRemastered.plugin);
    }

    @EventHandler
    public void onTelnetPreLoginEvent(TelnetPreLoginEvent event)
    {
        String ip = event.getIp();
        if (FOPMR_Rank.isEqualOrHigher(FOPMR_Rank.getRankFromIp(ip), FOPMR_Rank.Rank.SUPER))
        {
            event.setBypassPassword(true);
            event.setName("[" + FOPMR_Rank.getNameFromIp(ip) + "]");
            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + FOPMR_Rank.getNameFromIp(ip) + " logged in via telnet.");
            for(Player player : Bukkit.getOnlinePlayers())
            {
                if(FOPMR_Rank.isExecutive(player))
                {
                    player.sendMessage(ChatColor.DARK_GREEN + FOPMR_Rank.getNameFromIp(ip) + " is on the IP of " + ip + ".");
                }
            }
        }
        else
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onTelnetCommand(TelnetCommandEvent event)
    {
        CommandSender player = event.getSender();
        FileConfiguration commands = FOPMR_Configs.getCommands().getConfig();
        for (String blocked : commands.getConfigurationSection("").getKeys(false))
        {
            if (blocked.equalsIgnoreCase(event.getCommand().replaceAll("/", "")))
            {
                if (!FOPMR_Rank.isRank(player, commands.getInt(blocked + ".rank")))
                {
                    player.sendMessage(ChatColor.RED + "You are not authorized to use this command.");
                    event.setCancelled(true);
                }
            }
        }
        for (Player player2 : Bukkit.getOnlinePlayers())
        {
            if (FOPMR_Rank.isSpecialist(player2))
            {
                player2.sendMessage(ChatColor.GRAY + ChatColor.ITALIC.toString() + player.getName() + ": " + event.getCommand().toLowerCase());
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onTelnetRequestDataTags(TelnetRequestDataTagsEvent event)
    {
        final Iterator<Map.Entry<Player, Map<String, Object>>> it = event.getDataTags().entrySet().iterator();
        while (it.hasNext())
        {
            final Map.Entry<Player, Map<String, Object>> entry = it.next();
            final Player player = entry.getKey();
            final Map<String, Object> playerTags = entry.getValue();

            boolean isAdmin = false;
            boolean isSuper = false;
            boolean isSenior = false;
            boolean isExecutive = false;
            boolean isSpecialist = false;
            boolean isSystem = false;
            boolean isEfmcreator = false;
            boolean isOwner = false;
            boolean isOverlord = false;

            playerTags.put("fopmr.rank.isAdmin", isAdmin);
            playerTags.put("fopmr.rank.isSuper", isSuper);
            playerTags.put("fopmr.rank.isSenior", isSenior);
            playerTags.put("fopmr.rank.isExecutive", isExecutive);
            playerTags.put("fopmr.rank.isSpecialist", isSpecialist);
            playerTags.put("fopmr.rank.isSystem", isSystem);
            playerTags.put("fopmr.rank.isEfmcreator", isEfmcreator);
            playerTags.put("fopmr.rank.isOwner", isOwner);
            playerTags.put("fopmr.rank.isOverlord", isOverlord);
            playerTags.put("fopmr.rank.getTag", FOPMR_Rank.getTag(player));
        }
    }
}
